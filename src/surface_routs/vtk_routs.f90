!
!      surf_quadratic_msh_vtk_plot - generate a vtk file to
!        plot a quadratic mesh corresponding to discretization
!      
!      surf_flat_msh_vtk_plot - generate a vtk file to plot
!        a flat mesh corresponding to discretization
!
!      surf_vtk_plot - generate a vtk file to plot the surface, 
!         scalar plotted is z coordinate
!
!      surf_vtk_plot_scalar - generate a vtk file to plot the surface,
!         along with prescribed scalar
!
!      vtk_write_plane - write a structured grid of data defined on
!       a plane
!
subroutine surf_quadratic_msh_vtk_plot(npatches,norders,ixyzs,iptype, &
  npts,srccoefs,srcvals,fname,title)
!
! This subroutine writes a vtk file to plot quadratic mesh 
! corresponding to discretization
!
! Currently only supports triangulation
!
!  Input arguments:
!    - npatches: integer
!        number of patches
!    - norders: integer(npatches)
!        order of discretization of each patch
!    - ixyzs: integer(npatches+1)
!        starting location in srccoefs,srcvals array where data for
!        patch i begins
!    - iptype: integer(npatches)
!        type of patch
!        iptype = 1, triangular patch with RV nodes
!    - npts: integere
!        number of points in discretization
!    - srccoefs: real *8 (9,npts)
!        xyz,dxyz/du,dxyz/dv koornwinder expansion coeffs for all patches
!    - srcvals: real *8 (12,npts)
!        xyz,dxyz/du,dxyz/dv,normals patch info 
!    - fname: character (len=*)
!        file name where vtk output should be written
!
  implicit none
  integer, intent(in) :: npatches,norders(npatches),ixyzs(npatches+1)
  integer, intent(in) :: iptype(npatches),npts
  real *8, intent(in) :: srccoefs(9,npts),srcvals(12,npts)
  character (len=*), intent(in) :: fname,title

  real *8 uvs(2,6)
  real *8, allocatable :: xyzs(:,:),pols(:)

  integer i,j,k,l,n0,npuv,ipatch,ipt,i1,m,norder,npols,iunit1

  uvs(1,1) = 0.0d0
  uvs(2,1) = 0.0d0
  
  uvs(1,2) = 1.0d0
  uvs(2,2) = 0.0d0

  uvs(1,3) = 0.0d0
  uvs(2,3) = 1.0d0

  uvs(1,4) = 0.5d0
  uvs(2,4) = 0.0d0
  
  uvs(1,5) = 0.5d0
  uvs(2,5) = 0.5d0

  uvs(1,6) = 0.0d0
  uvs(2,6) = 0.5d0

  npuv = 6

  n0 = npatches*npuv
  allocate(xyzs(3,n0))

  do ipatch=1,npatches
    npols = ixyzs(ipatch+1)-ixyzs(ipatch)
    norder = norders(ipatch)
    allocate(pols(npols))
    do j=1,npuv
      call koorn_pols(uvs(1,j),norder,npols,pols)
      ipt = (ipatch-1)*npuv + j
      do m=1,3
        xyzs(m,ipt) = 0
      enddo

      do l=1,npols
        do m=1,3
          xyzs(m,ipt) = xyzs(m,ipt) + &
             pols(l)*srccoefs(m,ixyzs(ipatch)+l-1)
        enddo
      enddo
    enddo
    deallocate(pols)
  enddo
  
  iunit1 = 877
  open(unit = iunit1, file=trim(fname), status='replace')

  write(iunit1,'(a)') "# vtk DataFile Version 3.0"
  write(iunit1,'(a)') trim(title)
  write(iunit1,'(a)') "ASCII"
  write(iunit1,'(a)') "DATASET UNSTRUCTURED_GRID"
  write(iunit1,'(a,i8,a)') "POINTS ", n0, " float"

  do i = 1,n0
    write(iunit1,"(E11.5,2(2x,e11.5))") xyzs(1,i), xyzs(2,i), xyzs(3,i)
  end do

  write(iunit1,'(a,i8,i8)') "CELLS ", npatches, npatches*7

  do ipatch=1,npatches
    i1 = 6*(ipatch-1) 
    write(iunit1,'(a,i8,i8,i8,i8,i8,i8)') "6 ", i1, i1+1,i1+2, &
      i1+3,i1+4,i1+5
  enddo

  write(iunit1,'(a,i8)') "CELL_TYPES ", npatches
  do ipatch = 1,npatches
    if(iptype(ipatch).eq.1) then
      write(iunit1,'(a)') "22"
    endif
  end do

  write(iunit1,'(a)') ""
  write(iunit1,'(a,i8)') "CELL_DATA ", npatches
  write(iunit1,'(a)') "SCALARS material int"
  write(iunit1,'(a)') "LOOKUP_TABLE default"
  do i = 1,npatches
    write(iunit1,'(a)') "1"
  end do

  close(iunit1)

end subroutine surf_quadratic_msh_vtk_plot
!
!
!
!
!
subroutine surf_vtk_plot(npatches,norders,ixyzs,iptype,npts,srccoefs,&
   srcvals,fname,title)
!
!
!
!
!  This subroutine writes a vtk file to plot the surface
!  
!f2py intent(in) npatches,norders,ixyzs,iptype,npts,srccoefs
!f2py intent(in) srcvals,fname,title
!
   implicit none
   integer npatches,norders(npatches),ixyzs(npatches+1),npts
   integer iptype(npatches)
   real *8 srccoefs(9,npts),srcvals(12,npts)
   real *8, allocatable :: sigma(:)
   character (len=*) fname,title

   integer i
   
   allocate(sigma(npts))
!$OMP PARALLEL DO DEFAULT(SHARED)   
   do i=1,npts
     sigma(i) = srcvals(3,i)
   enddo
!$OMP END PARALLEL DO

  call surf_vtk_plot_scalar(npatches,norders,ixyzs,iptype,npts,  &
    srccoefs,srcvals,sigma,fname,title)

end subroutine surf_vtk_plot
!
!
!
!
!
!
subroutine surf_vtk_plot_scalar(npatches,norders,ixyzs,iptype, &
  npts,srccoefs,srcvals,sigma,fname,title)
!
!   This subroutine writes a vtk to plot the surface along
!   with a scalar. Currently only supports triangular patches
!
!
!f2py intent(in) npatches,norders,ixyzs,iptype,npts,srccoefs
!f2py intent(in) srcvals,sigma,fname,title
!
  implicit none
  integer npatches,norders(npatches),ixyzs(npatches+1),npts
  integer iptype(npatches)
  real *8 srccoefs(9,npts),srcvals(12,npts),sigma(npts)
  real *8, allocatable :: sigma_coefs(:),pols(:),rtmp(:)
  character (len=*) fname,title

  real *8, allocatable :: xyzs(:,:),uvs(:,:,:),splot(:)
  integer, allocatable :: kovers(:),nps(:),ipstart(:)

  integer i,j,k,l,ipatch,npout,kover,npols
  integer itrip,itric1,nb,nlmax,nuv,istart,iend,nd
  integer ilstart,itri,iunit1,m,ncell,ncsize,norder,nuvl,i1

  real *8 ra,erra

!
!  get the coefs of the density
!
   nd = 1
   allocate(sigma_coefs(npts))
   call surf_vals_to_coefs(nd,npatches,norders,ixyzs,iptype,npts, &
     sigma,sigma_coefs)


 
!
!   estimate kovers, nps 
!

  npout = 0
  allocate(kovers(npatches),nps(npatches),ipstart(npatches+1))
  do i=1,npatches
    npols = ixyzs(i+1)-ixyzs(i)
    kover = 0
    if(npols.gt.4**0) kover = 1
    if(npols.gt.4**1) kover = 2
    if(npols.gt.4**2) kover = 3
    if(npols.gt.4**3) kover = 4
    kovers(i) = kover
    nps(i) = 4**kover
    if(iptype(i).eq.1) nps(i) = nps(i)*3
    npout = npout + nps(i) 
  enddo


  ipstart(1) = 1
  nps(1) = nps(1) + 1
  call cumsum(npatches,nps,ipstart(2))
  nps(1) = nps(1) - 1

  allocate(xyzs(3,npout),splot(npout))

!
!   get uvs of all patches of type = 1
!
  
  nlmax = 2
  nuv = (4**(nlmax+1)-1)/3
  allocate(uvs(2,3,nuv))

  uvs(1,1,1) = 0
  uvs(2,1,1) = 0
  uvs(1,2,1) = 1
  uvs(2,2,1) = 0
  uvs(1,3,1) = 0
  uvs(2,3,1) = 1

  do i=0,nlmax-1
    istart = (4**(i)-1)/3+1
    nb = 4**i
    iend = istart + nb-1
    do itrip = istart,iend
      itric1 = (itrip-istart)*4 + iend
      call gettrichildren(uvs(1,1,itrip),uvs(1,1,itric1+1), &
       uvs(1,1,itric1+2),uvs(1,1,itric1+3),uvs(1,1,itric1+4))   
    enddo
  enddo


  do ipatch=1,npatches
    istart = ipstart(ipatch)
    npols = ixyzs(ipatch+1)-ixyzs(ipatch)
    norder = norders(ipatch)
    allocate(pols(npols))
    if(iptype(ipatch).eq.1) then

      nuvl = ipstart(ipatch+1)-ipstart(ipatch)
      ilstart = 4**(kovers(ipatch)-1)/3+1
      nb = 4**(kovers(ipatch))
      do i=1,nb
        itri = i+ilstart-1
        do j=1,3
          call koorn_pols(uvs(1,j,itri),norder,npols,pols)
          
          do m=1,3
            xyzs(m,istart+3*(i-1)+j-1) = 0
          enddo
          splot(istart+3*(i-1)+j-1) = 0

          do l=1,npols
            do m=1,3
              xyzs(m,istart+3*(i-1)+j-1) = & 
                xyzs(m,istart+3*(i-1)+j-1) + &
                pols(l)*srccoefs(m,ixyzs(ipatch)+l-1)
            enddo
            splot(istart+3*(i-1)+j-1) = &
             splot(istart+3*(i-1)+j-1)+ &
             pols(l)*sigma_coefs(ixyzs(ipatch)+l-1)
          enddo
        enddo
      enddo
    endif
    deallocate(pols)
  enddo
  
  iunit1 = 877
  open(unit = iunit1, file=trim(fname), status='replace')

  write(iunit1,'(a)') "# vtk DataFile Version 3.0"
  write(iunit1,'(a)') trim(title)
  write(iunit1,'(a)') "ASCII"
  write(iunit1,'(a)') "DATASET UNSTRUCTURED_GRID"
  write(iunit1,'(a,i8,a)') "POINTS ", npout, " float"

  do i = 1,npout
    write(iunit1,"(E11.5,2(2x,e11.5))") xyzs(1,i), xyzs(2,i), xyzs(3,i)
  end do

  ncell = 0
  ncsize = 0
  do i=1,npatches
    ncell = ncell + 4**kovers(i)
    if(iptype(i).eq.1) ncsize = ncsize + 4*(4**kovers(i))
  enddo

  write(iunit1,'(a,i8,i8)') "CELLS ", ncell, ncsize

  do ipatch=1,npatches
    nb = 4**kovers(ipatch)
    if(iptype(ipatch).eq.1) then
      istart = ipstart(ipatch) 
      do i = 1,nb
        i1 = istart + 3*(i-1) 
        write(iunit1,'(a,i8,i8,i8)') "3 ", i1-1, i1, i1+1
      enddo
    endif
  end do

  write(iunit1,'(a,i8)') "CELL_TYPES ", ncell
  do ipatch = 1,npatches
    nb = 4**kovers(ipatch)
    if(iptype(ipatch).eq.1) then
      do i=1,nb
        write(iunit1,'(a)') "5"
      enddo
    endif
  end do

  write(iunit1,'(a)') ""
  write(iunit1,'(a,i8)') "POINT_DATA ", npout
  write(iunit1,'(a,i4)') "SCALARS scalar_function float ", 1
  write(iunit1,'(a)') "LOOKUP_TABLE default"
  do i = 1,npout
    write(iunit1,'(E11.5)') splot(i)
  end do

  close(iunit1)



end subroutine surf_vtk_plot_scalar
!
!
!
!
!
!
subroutine vtk_write_plane(ndims,ntarg,xyz,dxyz,f,title,fname)
!
!   writes data for a structured grid on a plane in vtk format
!
  implicit none
  integer ndims(3),ntarg,i,iunit1
  real *8 xyz(3),dxyz(3),f(ntarg)
  character (len=*) fname,title

  print *, "Here - in vtk_write_plane"
  

  iunit1 = 877
  open(unit = iunit1, file=trim(fname), status='replace')

  write(iunit1,'(a)') "# vtk DataFile Version 3.0"
  write(iunit1,'(a)') trim(title)
  write(iunit1,'(a)') "ASCII"
  write(iunit1,'(a)') "DATASET STRUCTURED_POINTS"
  write(iunit1,'(a,i4,i4,i4)') "DIMENSIONS ", ndims(1),ndims(2),ndims(3)
  write(iunit1,'(a,e11.5,1x,e11.5,1x,e11.5)') "ORIGIN ", &
    xyz(1),xyz(2),xyz(3)
  write(iunit1,'(a,e11.5,1x,e11.5,1x,e11.5)') "SPACING ", &
    dxyz(1),dxyz(2),dxyz(3)
  write(iunit1,'(a)') ""
  write(iunit1,'(a,i8)') "POINT_DATA ",ntarg 
  write(iunit1,'(a,i4)') "SCALARS scalar_function float ", 1
  write(iunit1,'(a)') "LOOKUP_TABLE default"
  do i = 1,ntarg
    write(iunit1,'(E11.5)') f(i)
  end do
  close(iunit1)



end subroutine vtk_write_plane
